import config from "./config";
import express from "express";

async function startServer() {
  const app = express();
  app.listen(config.port, () => {
    console.log("Hello World!");
  });
}

startServer();
