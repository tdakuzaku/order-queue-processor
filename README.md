# Order Queue Processor

Create an API/service that will process a queue of pending orders for a marketplace by generating payment orders for customers and transfers for partners/affiliates.
In the file you received with this test there are two mock-up APIs created using JSONPlaceholder, these are REST APIs that you will use in order to create/fetch invoices and create transfers.
There is also a data seed file for you to create your order queue, you may store this data in a database.

1. processing the order queue
2. fetching data about a specific order
3. creating an order.

When processing the order queue, consider the following:
1. If an order has a status of “Created”, you should create an invoice for it, updating the order data with the invoice ID and changing the status to “Charged”.
2. If an order has a status of “Charged”, you should create a transfer, updating the order data with the transfer ID and changing the status to “Completed”.
When fetching data about an order, if available, populate the data for the Invoice and Transfer.
When creating an order, don’t allow transfer and invoice to be set.
Unit and integration tests are welcome.